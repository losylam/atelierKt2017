var largeur = 1900;
var compteur = 0;
var position_x = -50;
var position_y = 50;
var vitesse = 10;

function setup() { 
    createCanvas(largeur, 100, SVG);
    background(255);
    noFill();
} 

function draw() {
    while(position_y<500){
		push();
   	 	stroke(0);
    	position_x = position_x + vitesse;
    	compteur = compteur + 1;
    	if (position_x > largeur+50){
	  		position_x = -50;
	  		position_y = position_y + 90;
    	}
    	translate(position_x, position_y);
    	rotate(cos(compteur/10)*3.14/4);

    	line(-50, 0, 50, 0);
//	rect(-25, -25, 50, 50);
	pop();
    }
  	save();
	noLoop();
  	b_save = false;
}

function keyPressed() {
    if (keyCode === ENTER) {
		b_save = true;
    }
}
