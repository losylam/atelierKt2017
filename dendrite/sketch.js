var largeur = 1920;
var hauteur = 1080/2;
var balles = [];
var cpt = 0;
var b_save = false;

function setup() { 
    createCanvas(largeur, hauteur);
    fill("#000");
    noStroke();
    angleMode(DEGREES);
    balles.push(new Particle());
    balles[0].v_x = 0;
    balles[0].v_y = 0;
    balles[0].x = largeur;
    balles[0].y = hauteur/2;
    for (var i = 0; i < 100; i++){
    	balles.push(new Particle());
    }
} 

function draw() { 
    background("fff");
    for (var i = 0; i< balles.length; i++){
	if ((balles[i].v_x != 0) && (balles[i].v_y != 0)){
	    balles[i].update();
	    for (var j = 0; j < balles.length; j++){
		if ((i!=j) && (balles[j].v_x==0) && (balles[j].v_y == 0)) {
		    var dd = balles[i].r + balles[j].r
		    if (dist(balles[i].x, balles[i].y, balles[j].x, balles[j].y) < dd/2){	
			balles[i].v_x = 0;
			balles[i].v_y = 0;
		    }
		} 
	    }
	}else{

	}
    balles[i].draw();	
    }
    cpt +=1;
    
    if ((balles.length<4000) && cpt%3 == 0){
	balles.push(new Particle());
    }

    if (b_save){
        noLoop();
	save();
	b_save = false;
    }
}


function Particle() {
    this.r = random([8, 8, 8, 8, 13, 13, 13, 17, 22]);
    if (random([true, false])){
	this.x = random(0, largeur);
	this.y = 1;
    }else{
	this.x = 1;
	this.y = random(0, hauteur);
    }

    this.v_x = random(3, 6);
    this.v_y = random(3, 6)*random([1, -1]);;

    this.draw = function(){
    	ellipse(this.x, this.y, this.r);
    };
    
    this.update = function(){
    	this.x += this.v_x;
    	this.y += this.v_y;
    	if (this.x > largeur){
    	    this.x = 0;
    	}else if(this.x < 0){
    	    this.x = largeur;
    	}
    	if (this.y > hauteur){
    	    this.y = 0;
    	}else if(this.y < 0){
    	    this.y = hauteur;
    	}
    };
    
}


function mousePressed(){
    balles.push(new Particle());
}

function keyPressed() {
    if (keyCode === ENTER) {
	b_save = true;
    }
}
